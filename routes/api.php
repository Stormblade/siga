<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user',

function (Request $request) {
		return $request->user();
	});

Route::get('test', function () {
		event(new siga\Events\StatusLiked('Someone'));
		return "Event has been sent!";
	});

Route::post('apiLogin', [
		'as'   => 'api-login',
		'uses' => 'servicios\ApiAuthController@postLogin'
	]);

Route::post('cargosService', [
		'as'   => 'api-login',
		'uses' => 'servicios\ServiciosController@listarCargos'
	]);

Route::post('apiCrearUsuario', [
		'as'   => 'api-crear',
		'uses' => 'servicios\ApiAuthController@crear_usuario'
	]);

Route::get('listarProveedores/{id}', 'servicios\ServiciosController@listarProveedores');
Route::post('registrarProveedores', 'servicios\ServiciosController@registrarProveedor');
Route::get('listarAcopios/{id}', 'servicios\ServiciosController@listarAcopio');
Route::get('registrarAcopios', 'servicios\ServiciosController@listarAcopio');

Route::group(array('middleware' => 'jwt.auth'), function () {
		Route::post('v.1.0/siga/proveedorListar', 'servicios\ServiciosController@listarProveedores');
		Route::post('v.1.0/siga/proveedorRegistrar', 'servicios\ServiciosController@registrarProveedor');
		Route::get('apiCerrar', [

				'as'   => 'cerrarApi',
				'uses' => 'servicios\ApiAuthController@logout'
			]);
	});

Route::post('listaPrueba', [
		'as'   => 'prueba-listar',
		'uses' => 'servicios\ServiciosController@listarPrueba'
	]);