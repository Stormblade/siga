<?php

namespace siga\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use siga\Modelo\insumo\Stock;
use siga\Modelo\insumo\Stock_Historial;
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        
        //$stock = Stock::get();
        
        $schedule->call(function (){
           
            $stock = Stock::get();
            foreach ($stock as $st) {
                // dd($st->stockal_id);
                DB::table('insumo.stock_historial')->insert(
                    ['his_stock_ins_id' => $st->stock_ins_id, 'his_stock_planta_id' => $st->stock_planta_id, 'his_stock_cant' => $st->stock_cantidad,'his_stock_cant_ingreso'=>0,'his_stock_cant_salida'=>0,'his_stock_usr_id'=>'1','his_stock_estado'=>'A']
                );
            }
       
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
