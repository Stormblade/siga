  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>Sistema Gestion de Almacenes</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="img/siga_logo_color.png">
     <?php echo Html::style('css/preload.min.css'); ?>

     <?php echo Html::style('css/bootstrap.css'); ?>

     <?php echo Html::style('css/plugins.min.css'); ?>

     <?php echo Html::style('css/style.light-blue-500.min.css'); ?>

     <?php echo Html::style('css/width-boxed.min.css'); ?>

  </head>