<?php $menu = app('siga\Http\Controllers\MenuController'); ?>
<aside>
    <div class="nav-collapse " id="sidebar">
        <ul class="sidebar-menu" id="nav-accordion">
            <p class="centered">
                <a href="<?php echo e(url('/home')); ?>">
                    <img class="img-rounded" src="/img/LogoEBA2019.png" width="150"/>
                </a>
            </p>
            <h5 class="centered">
                SISTEMA GESTION DE ALMACENES - EBA<br>
                PLANTA: <?php echo e(Session::get('PLANTA')); ?> <br>
                <?php echo e(Auth::User()->usr_usuario); ?>

            </h5>
             <?php $__currentLoopData = $menu->submenus(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link01): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php
                    $grupo01=0;
                    $grupo01= $link01->grp_id;
                ?>
            <li class="sub-menu">
                <a href="#">
                    <i class="<?php echo e($link01->grp_imagen); ?>" aria-hidden="true"></i>
                    <span>
                        <?php echo e($link01->grp_grupo); ?>

                    </span>
                </a>
                <ul class="sub">
                    <?php $__currentLoopData = $menu->links($grupo01); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link02): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                    <li>
                        <a href="<?php echo e(url($link02->opc_contenido)); ?>">
                            <i class="fa fa-arrow-right"></i><?php echo e($link02->opc_opcion); ?>

                        </a>
                    </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>   
        </ul>
    </div>
</aside>
<?php $__env->startPush('scripts'); ?>
<script>
</script>
<?php $__env->stopPush(); ?>