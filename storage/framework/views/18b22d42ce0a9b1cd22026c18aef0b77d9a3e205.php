<?php $__env->startSection('main-content'); ?>
<?php echo $__env->make('backend.administracion.insumo.insumo_devolucion.devolucion_recibidas.partials.modalCreate', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-default box-solid">
            <div class="box-header with-border">
            <div class="col-md-12">
                <div class="col-md-1">
                    <a type="button" class="btn btn-dark"  style="background: #000000;" href="<?php echo e(url('DevolucionRegistrosMenu')); ?>"><span class="fa fas fa-align-justify" style="background: #ffffff;"></span><h7 style="color:#ffffff">&nbsp;&nbsp;MENU</h7></a>
                </div>
                <div class="col-md-8">
                     <h4><label for="box-title">LISTA DE DEVOLUCIONES RECIBIDAS</label></h4>
                </div>
                <div class="col-md-2">
                </div>
                <div class="col-md-2">
               
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border"></div>
                    <div class="box-body">
                      <table class="col-md-12 table-bordered table-striped table-condensed cf" id="lts-devolucionReci">
                            <thead class="cf">
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Operaciones
                                    </th>
                                    <th>
                                        N° Devoluciones
                                    </th>
                                    <th>
                                        Fecha
                                    </th>
                                </tr>
                            </thead>
                            <tr>
                            </tr>
                    </table>
                </div>    
            </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<script>
    var t = $('#lts-devolucionReci').DataTable( {
            "destroy":true,
            "processing": true,
            "serverSide": true,
            "ajax": "/DevolucionRecibida/create/",
            "columns":[
                {data: 'dev_id'},
                {data: 'acciones',orderable: false, searchable: false},
                {data: 'numdev'},
                {data: 'dev_registrado'},
            ],
        "language": {
             "url": "/lenguaje"
        },
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
         "order": [[ 0, "desc" ]]
       
    });
t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
 function MostrarDevolucionRec(id){     
    var dat=id.value;
    console.log(dat);
      $('#lts-devolucionDetalleRec').DataTable( {
            "destroy":true,
            "processing": true,
            "serverSide": true,
            "ajax": "DevolucionDetalleRec/"+dat,
            "columns":[
                {data: 'id'},
                {data: 'insumo'},
                {data: 'unidad'},
                {data: 'cantidad'},
                {data: 'rango'},
                {data: 'adicion'},
                {data: 'devolucion'},
        ],
        "order": [[ 0, "asc" ]],
        "language": {
             "url": "/lenguaje"
        },
         "lengthMenu": [[5, 25, 50, -1], [10, 25, 50, "All"]],
         "order": [[ 0, "desc" ]]
    });
   }

 function MostrarDatos(btn){
      var route = "/DevolucionRecibida/"+btn.value+"/edit";
      $.get(route, function(res){
        $("#dev_id1").val(res.dev_id);
        $("#nom_rec1").val(res.dev_nom_rec);
        $("#num_salida1").val(res.dev_num_sal);
        $("#dev_solicitante").val(res.prs_nombres+' '+res.prs_paterno+' '+ res.prs_materno);
      });
    }

 $("#aprobDevoluciones").click(function(){
    var rows = [];
        $('#lts-devolucionDetalleRec tbody tr').each(function(){
            rows.push({
                id1:$(this).find("td").eq(0).html(),
                insumo1:$(this).find("td").eq(1).html(),
                unidad1:$(this).find("td").eq(2).html(),
                cantidad1:$(this).find("td").eq(3).html(),
                rango1:$(this).find("td").eq(4).html(),
                adicion1:$(this).find("td").eq(4).html(),
                devolucion1:$(this).find("td").eq(4).html(),
            });
        });
            itemsDevoluciones = JSON.stringify(rows);
            console.log('Json Table devolucion: ', itemsDevoluciones);
          
            var route="/DevolucionRecibida";
                     var token =$("#token").val();
                     $.ajax({
                         url: route,
                         headers: {'X-CSRF-TOKEN': token},
                         type: 'POST',
                         dataType: 'json',
                         data: {
                         'devrec_id_dev':$("#dev_id1").val(),
                         'devrec_nom_receta':$("#nom_rec1").val(),
                         'devrec_num_salida':$("#num_salida1").val(),
                        // 'devrec_tipo_sol':$("#dev_solicitante").val(),
                         'data':itemsDevoluciones,
                        // 'obs':$("#obs").val(),
                         },
                         success: function(data){
                            // $('#lts-carrito').DataTable().ajax.reload();
                             swal("Devolucion!", "aprobado correctamente","success");
                             $("#myCreateRecibidas").modal('toggle');Limpiar();
                         },
                         error: function(result)
                         {
                         swal("Opss..!", "Error al registrar el dato", "error");
                         }
                     });
    });
   

</script>
<?php $__env->stopPush(); ?>



<?php echo $__env->make('backend.template.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>