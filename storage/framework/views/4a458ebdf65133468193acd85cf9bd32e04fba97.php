<div class="modal fade modal-primary" data-backdrop="static" data-keyboard="false" id="formEvaluacion" tabindex="-5">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel">
                    Evaluacion Proveedor
                </h4>
            </div>
            <div class="modal-body">
                <div class="caption">
                        <?php echo Form::open(['id'=>'proveedor']); ?>

                        <input id="token" name="csrf-token" type="hidden" value="<?php echo e(csrf_token()); ?>">
                            <input id="prov_id_eval" name="provid" type="hidden" value="">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Nombre Proveedor:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    <?php echo Form::text('nombre', null, array('placeholder' => 'Ingrese Nombre','class' => 'form-control','id'=>'nombre_proveedor', 'style'=>'text-transform:uppercase;', 'onkeyup'=>'javascript:this.value=this.value.toUpperCase();')); ?>

                                                </span>  
                                            </div>
                                        </div>
                                    </div>
                                    
                                 
                                </div>
                                <br>  
                                <strong><h5 class="modal-title" style="color:#8a6d3b">Evaluación</h5></strong>       
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Seleccione una opción:
                                                </label> 
                                                <select id="evaluacion" name="evaluacion" class="form-control">
                                                    <option value="1">Entrego muy tarde</option>
                                                    <option value="2">Entrego productos dañados</option>
                                                    <option value="3">No Entrego</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </input>
                        </input>
                    </hr>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" type="button">
                    Cerrar
                </button>
                <?php echo link_to('#',$title='Registrar Evaluación', $attributes=['id'=>'registroProvEval','class'=>'btn btn-success','style'=>''], $secure=null); ?>

                <?php echo Form::close(); ?>

            </div>
        </div>
    </div>
</div>

 

