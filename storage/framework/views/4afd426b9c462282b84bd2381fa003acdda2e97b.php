<?php $__env->startSection('main-content'); ?>
<?php echo $__env->make('backend.administracion.insumo.insumo_registro.ufv.partials.modalCreate', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('backend.administracion.insumo.insumo_registro.ufv.partials.modalUpdate', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-default box-solid">
            <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script> 
            
            <div class="box-header with-border">
            <div class="col-md-12"  id="contenido">
                <?php  
                 date_default_timezone_set('America/New_York');
                 $fechact=date('Y-m-d');
                 //echo $dat;
                // echo $fechact;
                 if($fechact==$fecha)
                 { 
                    /*echo  '<div class="hidden" id="contenido">';
                    echo'<button id="reg" class="btn pull-right btn-default" style="background: #616A6B"  data-target="#myCreateUfv" data-toggle="modal"><h6 style="color: white;">+&nbsp;NUEVO REGISTRO</h6> </button>' ;
                    echo '</div>';*/
                 }
                 else
                 {
                    /*echo  '<div>';
                    echo'<button id="reg" class="btn pull-right btn-default" style="background: #616A6B" data-target="#myCreateUfv" data-toggle="modal"><h6 style="color: white;">+&nbsp;NUEVO REGISTRO</h6> </button>' ;
                    echo '</div>';*/
                 }
                ?>
                <div class="col-md-1">
                    <a type="button" class="btn btn-dark"  style="background: #000000;" href="<?php echo e(url('InsumoRegistrosMenu')); ?>"><span class="fa fas fa-align-justify" style="background: #ffffff;"></span><h7 style="color:#ffffff">&nbsp;&nbsp;MENU</h7></a>
                </div>  
                <div class="col-md-8">
                     <h4><label for="box-title">LISTA DE REGISTRO UFV</label></h4>
                </div>
                
                <div>
                <a href="<?php echo e(url('ReporteUfvExcel')); ?>" class="btn pull-right btn-success" data-target="" data-toggle="modal"><h6 style="color: white;"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>&nbsp;EXPORTAR EXCEL</h6></a>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border"></div>
                    <div class="box-body">
                        <table class="col-md-12 table-bordered table-striped table-condensed cf" id="lts-ufv" style="width:100%">
                            <thead class="cf">
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <!--<th>
                                        Operaciones
                                    </th>-->
                                    <th>
                                        Fecha
                                    </th>
                                    <th>
                                        Valor UFV
                                    </th>
                                </tr>
                            </thead>
                            <tr>
                            </tr>
                    </table>
                </div>    
            </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<script>
    var t = $('#lts-ufv').DataTable( {
      
         "processing": true,
            "serverSide": true,
            "ajax": "/UfvInsumo/create/",
            "columns":[
                {data: 'ufv_id'},
                //{data: 'acciones',orderable: false, searchable: false},
                {data: 'ufv_registrado'},
                {data: 'ufv_cant'},
        ],
        
        "language": {
             "url": "/lenguaje"
        },
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
         "order": [[ 0, "desc" ]],
       
    });

    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    function Limpiar(){
        $("#cantidad").val("");
    }

    $("#registroUfv").click(function(){
        var route="/UfvInsumo";
        var token =$("#token").val();
        $.ajax({
            url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data: {
                //'ufv_registrado':$("#fechareg").val(),
                'ufv':$("#cantidad").val(),
                },
                success: function(data){
                    $("#myCreateUfv").modal('toggle');Limpiar();
                    swal("Ufv!", "registro correcto","success");
                    $('#lts-ufv').DataTable().ajax.reload();
                    location.reload('/UfvInsumo');
                    
                },
                error: function(result)
                {
                // swal("Opss..!", "Error al registrar el dato", "error");
                    var errorCompleto='Tiene los siguientes errores: ';
                    $.each(result.responseJSON.errors,function(indice,valor){
                       errorCompleto = errorCompleto + valor+' ' ;                       
                    });
                    swal("Opss..., Hubo un error!",errorCompleto,"error");
                }
            });
     });

        function MostrarUfv(btn){
            var route = "/UfvInsumo/"+btn.value+"/edit";
            $.get(route, function(res){
                $("#ufv_id1").val(res.ufv_id);
                $("#ufv_cant1").val(res.ufv_cant);
                $("#ufv_registrado1").val(res.ufv_registrado);
            });
        }

        $("#actualizarUfv").click(function(){
        var value = $("#ufv_id1").val();
        var route="/UfvInsumo/"+value+"";
        var token =$("#token").val();
        $.ajax({
            url: route,
            headers: {'X-CSRF-TOKEN': token},
            type: 'PUT',
            dataType: 'json',
            data: {
                    'ufv_cant':$("#ufv_cant1").val(),
                  },
                        success: function(data){
                $("#myUpdateUfv").modal('toggle');
                swal("Ufv!", "edicion exitosa!", "success");
                $('#lts-ufv').DataTable().ajax.reload();

            },  error: function(result) {
                  console.log(result);
                 swal("Opss..!", "Edicion rechazada", "error")
            }
        });
        });

        function Eliminar(btn){
        var route="/UfvInsumo/"+btn.value+"";
        var token =$("#token").val();
        swal({   title: "Eliminacion de registro?",
          text: "Uds. esta a punto de eliminar 1 registro",
          type: "warning",   showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Eliminar!",
          closeOnConfirm: false
        }, function(){
           $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    dataType: 'json',

                    success: function(data){
                        $('#lts-ufv').DataTable().ajax.reload();
                        swal("Ufv!", "El registro fue dado de baja!", "success");
                    },
                        error: function(result) {
                            swal("Opss..!", "error al procesar la solicitud", "error")
                    }
                });
        });
        }

        // $(document).ready(function() {
        //     var refreshId =  setInterval( function(){
        //     $('#contenido').load('backend.administracion.insumo.insumo_registro.ufv.index.blade.php');//actualizas el div
        //    }, 1000 );
        // });

</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('backend.template.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>