<?php $__env->startSection('main-content'); ?>
<?php echo Form::open(array('route' => 'RolUsuario.store','method'=>'POST','class'=>'')); ?>


<?php if(Session::has('message')): ?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
	<?php echo e(Session::get('message')); ?>

</div>
<?php endif; ?>

<h2>Usuario Rol</h2>

<div class="row">
	<div class="col-md-5">
		<div id="no-more-tables">
			<table class="col-md-12 table-striped table-condensed cf" id="lts-usuario" border="1" bordercolor="#999">
				<thead class="cf">
					<tr>
						<th colspan="4"> Usuarios </th>
					</tr>
					<tr>
						<th><i class="fa fa-check-square-o fa-2x"></i></th>
						<th>ID</th>
						<th>Nombre</th>
						<th>Clave</th>
					</tr>
				</thead>
				<?php $__currentLoopData = $rolUser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $u): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

				<tr>
					<td data-title="Seleccionar">
						<input tabindex="1" type="radio" name="usuario[]" id="<?php echo e($u->usr_id); ?>" value="<?php echo e($u->usr_id); ?>">
					</td>
					<td data-title="ID"><?php echo e($u->usr_id); ?></td>
					<td data-title="Nombre"><?php echo e($u->usr_usuario); ?></td>
					<td data-title="Clave"><?php echo e($u->usr_clave); ?></td>
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</table>
		</div>
	</div>
	<div class="col-md-6" >
		<div id="no-more-tables">
			<table class="col-md-12 table-striped table-condensed cf" id="rolasignado" border="1" bordercolor="#999">
				<thead class="cf">
					<tr>
						<th colspan="4"> Roles Asignados </th>
					</tr>
					<tr>
						<th><i class="fa fa-check-square-o fa-2x"></i></th>
						<th>ID</th>
						<th>Usuario</th>
						<th>Rol</th>
					</tr>
				</thead>
				<?php $__currentLoopData = $rolusuario; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ar): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr>
					<td data-title="Seleccionar">
						<input type="checkbox" name="rolasignado[]" id="<?php echo e($ar->usrls_id); ?>" value=" <?php echo e($ar->usrls_id); ?>">
					</td>
					<td data-title="ID"><?php echo e($ar->rls_id); ?></td>
					<td data-title="Usuario"><?php echo e($ar->usr_usuario); ?></td>
					<td data-title="Rol"><?php echo e($ar->rls_rol); ?></td>
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

			</table>
			&nbsp;
		</div>
		<div  align="center">
			<button type="submit" name="Asignar" class="btn btn-default" style="background:#61BC8C" ><span class="fa fa-chevron-left" ></span> Asignar</button>
			&nbsp; &nbsp;
			<button type="submit" name="Desasignar" class="btn btn-default" style="background:#61BC8C"><span class="fa fa-chevron-right" ></span>Desasignar</button>
		</div>&nbsp;
		<div class="col-lg-12" >
			<div id="no-more-tables">
				<table class="col-md-12 table-striped table-condensed cf" id="rolnoasignado"  border="1" bordercolor="#999">
					<thead class="cf">
						<tr>
							<th colspan="4"> Roles No Asignados </th>
						</tr>
						<tr>
							<th><i class="fa fa-check-square-o fa-2x"></i></th>
							<th>ID</th>
							<th>Usuario</th>
							<th>Rol</th>
						</tr>
					</thead>

					<?php $__currentLoopData = $rol; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr>
						<td data-title="Seleccionar">
							<input tabindex="1" type="checkbox" name="rolnoasignado[]" id="<?php echo e($r->rls_id); ?>" value=" <?php echo e($r->rls_id); ?>">
						</td>
						<td data-title="ID"><?php echo e($r->rls_id); ?></td>
						<td data-title="Usuario"><?php echo e($r->usr_usuario); ?></td>
						<td data-title="Rol"><?php echo e($r->rls_rol); ?></td>
					</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</table>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<script type="text/javascript">
    
    $('form').submit(function(e){
        // si la cantidad de checkboxes "chequeados" es cero,
        // entonces se evita que se envíe el formulario y se
        // muestra una alerta al usuario
        if ($('input[type=checkbox]:checked').length === 0) {
            e.preventDefault();
            alert('Debe seleccionar al menos una opcion');
        }
    });

</script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('backend.template.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>