<?php $__env->startSection('htmlheader_title'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('main-content'); ?>
<?php if(Session::has('message')): ?>
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <?php echo e(Session::get('message')); ?>

</div>
<?php endif; ?>
<?php if(Session::has('messageError')): ?>
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <?php echo e(Session::get('messageError')); ?>

</div>
<?php endif; ?>
<?php echo Form::open(array('route' => 'Asignacion.store','method'=>'POST','class'=>'')); ?>

<input type="hidden" name="rolacceso" value="<?php echo e($idrol); ?>">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="pull-left">
                    Accesos Ingreso al Menu
                </h3>
                <h3 class="pull-right">
                    <?php echo e($nombreRol); ?>

                </h3>
            </div>
            <div class="box-body">
                <div class="btn-group" role="group">
                    <div class="form-group">
                        <label>
                            Roles
                        </label>
                        <select class="form-control" name="rolos" id="rolid" onchange="cambioRol(this.value);">
                            <option value="">-Seleccione-</option>
                            <?php $__currentLoopData = $rol; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vaciado_rol): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($vaciado_rol->rls_id); ?>">
                                <?php echo e($vaciado_rol->rls_rol); ?>

                            </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>
                <div>
    <div id="no-more-tables">
        <table border="1" bordercolor="#999" class="col-md-4 table-striped table-condensed cf" id="lts-asignacion" style="">
            <thead class="cf">
                <tr>
                    <th colspan="4">
                        OPCIONES
                    </th>
                </tr>
                <tr>
                    <th>
                        <i class="fa fa-check-square-o fa-2x">
                        </i>
                    </th>
                    <th>
                        Grupo
                    </th>
                    <th>
                        Opcion
                    </th>
                    <th>
                        Contenido
                    </th>
                </tr>
            </thead>
            <?php $__currentLoopData = $opc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $o): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td data-title="Seleccionar">
                    <input id="<?php echo e($o->opc_id); ?>" name="opciones[]" tabindex="1" type="checkbox" value="<?php echo e($o->opc_id); ?>">
                    </input>
                </td>
                <td data-title="Grupo">
                    <?php echo e($o->grp_grupo); ?>

                </td>
                <td data-title="Opcion">
                    <?php echo e($o->opc_opcion); ?>

                </td>
                <td data-title="Contenido">
                    <?php echo e($o->opc_contenido); ?>

                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </table>
    </div>
    <!--botones-->
    <div class="col-md-2">
        <button class="btn btn-theme03" name="agregar" type="submit" value="agregar">
            Agregar
        </button>
        <button class="btn btn-theme04" name="retirar" type="submit" value="retirar">
            Retirar
        </button>
    </div>
    <!--botones-->
    <!-- este div es para la segunda tabla-->
            <div id="no-more-tables">
                <table border="1" bordercolor="#999" class="col-md-4 table-striped table-condensed cf" id="lts-asignacion2">
                    <thead class="cf">
                        <tr>
                            <th colspan="6">
                                ACCESOS
                            </th>
                        </tr>
                        <tr>
                            <th>
                                <i class="fa fa-check-square-o fa-2x">
                                </i>
                            </th>
                            <th>
                                Rol
                            </th>
                            <th>
                                Opcion
                            </th>
                            <th>
                                Contenido
                            </th>
                            <th>
                                Registrado
                            </th>
                            <th>
                                Modificado
                            </th>
                        </tr>
                    </thead>
                    <?php $__currentLoopData = $acceso; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td data-title="Seleccionar">
                            <input id="<?php echo e($a->acc_id); ?>" name="asignaciones[]" tabindex="1" type="checkbox" value=" <?php echo e($a->acc_id); ?>">
                            </input>
                        </td>
                        <td data-title="Rol">
                            <?php echo e($a->rls_rol); ?>

                        </td>
                        <td data-title="Opcion">
                            <?php echo e($a->opc_opcion); ?>

                        </td>
                        <td data-title="Contenido">
                            <?php echo e($a->opc_contenido); ?>

                        </td>
                        <td data-title="Contenido">
                            <?php echo e($a->acc_registrado); ?>

                        </td>
                        <td data-title="Contenido">
                            <?php echo e($a->acc_modificado); ?>

                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </table>
            </div>
            </div>
            </div>
        </div>
    </div>
</div>
<!-- este div es para la primera tabla-->
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<script>
    $(document).ready(function(){
        $("select").change(function(){
        var rol = $("#rolid").val();
        console.log("este es el rol",rol);
        $("#txtTabla").val(rol);
        $("#rls_id").submit();
        });
    });
    function cambioRol(idRol){
         window.location.href = '/AsignacionRol/'+idRol; //using a named route
    }
</script>
<?php $__env->stopPush(); ?>



<?php echo $__env->make('backend.template.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>