<?php $__env->startSection('main-content'); ?>
<?php echo $__env->make('backend.administracion.insumo.insumo_registro.proveedores.partials.modalCreate', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('backend.administracion.insumo.insumo_registro.proveedores.partials.modalUpdate', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('backend.administracion.insumo.insumo_registro.proveedores.partials.modalFormEvaluacion', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('backend.administracion.insumo.insumo_registro.proveedores.partials.modalListarEvaluacion', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-default box-solid">
            <div class="box-header with-border">
            <div class="col-md-12">
                <div class="col-md-1">
                    <a type="button" class="btn btn-dark"  style="background: #000000;" href="<?php echo e(url('InsumoRegistrosMenu')); ?>"><span class="fa fas fa-align-justify" style="background: #ffffff;"></span><h7 style="color:#ffffff">&nbsp;&nbsp;MENU</h7></a>
                </div>
                <div class="col-md-8">
                     <h4><label for="box-title">LISTA DE PROVEEDORES INSUMOS</label></h4>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-2">
                <button class="btn pull-right btn-default" style="background: #616A6B"  data-target="#myCreateProv" data-toggle="modal"><h6 style="color: white">+&nbsp;NUEVO PROVEEDOR</h6></button>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border"></div>
                    <div class="box-body">
                        <table class="col-md-12 table-bordered table-striped table-condensed cf" id="lts-proveedores">
                            <thead class="cf">
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Operaciones
                                    </th>
                                    <th>
                                        Proveedores
                                    </th>
                                    <th>
                                        Direccion
                                    </th>
                                    <th>
                                        Telefono
                                    </th>
                                    <th>
                                        Responsable
                                    </th>
                                    <th>
                                        Evaluación Proveedor
                                    </th>
                                </tr>
                            </thead>
                            <tr>
                            </tr>
                    </table>
                </div>    
            </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<script>
    var t =  $('#lts-proveedores').DataTable( {
      
         "processing": true,
            "serverSide": true,
            "ajax": "/ProveedorInsumo/create/",
            "columns":[
                {data: 'prov_id'},
                {data: 'acciones',orderable: false, searchable: false}, 
                {data: 'prov_nom'},
                {data: 'prov_dir'},
                {data: 'prov_tel'},
                {data: 'prov_nom_res'},
                {data: 'evaluacion_prov'}
        ],
        
        "language": {
             "url": "/lenguaje"
        },
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
         "order": [[ 0, "desc" ]]
       
    });
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

     function Limpiar(){
        $("#nombre").val("");
        $("#direccion").val("");
        $("#telefono").val("");
        $("#nomres").val("");
        $("#apres").val("");
        $("#amres").val("");
        $("#telres").val("");
        $("#obs").val("");
      }

        $("#registroProv").click(function(){
            var route="/ProveedorInsumo";
            var token =$("#token").val();
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data: {
                'nombre_proveedor':$("#nombre").val(),
                'prov_dir':$("#direccion").val(),
                'prov_tel':$("#telefono").val(),
                'nombre_responsable':$("#nomres").val(),
                'prov_ap_res':$("#apres").val(),
                'prov_am_res':$("#amres").val(),
                'prov_tel_res':$("#telres").val(),
                'prov_obs':$("#obs").val(),
                },
                success: function(data){
                    $("#myCreateProv").modal('toggle');Limpiar();
                    swal("Proveedor!", "registro correcto","success");
                    $('#lts-proveedores').DataTable().ajax.reload();
                },
                error: function(result)
                {
                // swal("Opss..!", "Error al registrar el dato", "error");
                    var errorCompleto='Tiene los siguientes errores: ';
                    $.each(result.responseJSON.errors,function(indice,valor){
                       errorCompleto = errorCompleto + valor+' ' ;                       
                    });
                    swal("Opss..., Hubo un error!",errorCompleto,"error");
                }
            });
        });

        function MostrarProv(btn){
            var route = "/ProveedorInsumo/"+btn.value+"/edit";
            $.get(route, function(res){
                $("#prov_id1").val(res.prov_id);
                $("#prov_nom1").val(res.prov_nom);
                $("#prov_dir1").val(res.prov_dir);
                $("#prov_tel1").val(res.prov_tel);
                $("#prov_nom_res1").val(res.prov_nom_res);
                $("#prov_ap_res1").val(res.prov_ap_res);
                $("#prov_am_res1").val(res.prov_am_res);
                $("#prov_tel_res1").val(res.prov_tel_res);
                $("#prov_obs1").val(res.prov_obs);
            });
        }

        $("#actualizarProv").click(function(){
        var value = $("#prov_id1").val();
        var route="/ProveedorInsumo/"+value+"";
        var token =$("#token").val();
        $.ajax({
            url: route,
            headers: {'X-CSRF-TOKEN': token},
            type: 'PUT',
            dataType: 'json',
            data: {
                    'prov_nom':$("#prov_nom1").val(),
                    'prov_dir':$("#prov_dir1").val(),
                    'prov_tel':$("#prov_tel1").val(),
                    'prov_nom_res':$("#prov_nom_res1").val(),
                    'prov_ap_res':$("#prov_ap_res1").val(),
                    'prov_am_res':$("#prov_am_res1").val(),
                    'prov_tel_res':$("#prov_tel_res1").val(),
                    'prov_obs':$("#prov_obs1").val(),
                  },
                        success: function(data){
                $("#myUpdateProv").modal('toggle');
                swal("Proveedor!", "edicion exitosa!", "success");
                $('#lts-proveedores').DataTable().ajax.reload();

            },  error: function(result) {
                  console.log(result);
                 swal("Opss..!", "Edicion rechazada", "error")
            }
        });
        });

        function Eliminar(btn){
        var route="/ProveedorInsumo/"+btn.value+"";
        var token =$("#token").val();
        swal({   title: "Eliminacion de registro?",
          text: "Uds. esta a punto de eliminar 1 registro",
          type: "warning",   showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Eliminar!",
          closeOnConfirm: false
        }, function(){
           $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    dataType: 'json',

                    success: function(data){
                        $('#lts-proveedores').DataTable().ajax.reload();
                        swal("Proveedor!", "El registro fue dado de baja!", "success");
                    },
                        error: function(result) {
                            swal("Opss..!", "error al procesar la solicitud", "error")
                    }
                });
        });
        }

        function FormEval(btn){
            var route = "/ProveedorInsumo/"+btn.value+"/edit";
            $.get(route, function(res){
                console.log(res);
                $('#nombre_proveedor').val(res.prov_nom);
                $('#prov_id_eval').val(res.prov_id);
            });
        }

        $("#registroProvEval").click(function(){
            var route="/EvaluacionProv";
            var token =$("#token").val();
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data: {
                    'eval_prov_id':$("#prov_id_eval").val(),
                    'eval_evaluacion':$("#evaluacion").val(),                
                },
                success: function(data){
                    $("#formEvaluacion").modal('toggle');
                    swal("Evaluación Proveedor!", "Registro correcto","success");
                    $('#lts-proveedores').DataTable().ajax.reload();
                },
                error: function(result)
                {
                    var errorCompleto='Tiene los siguientes errores: ';
                    $.each(result.responseJSON.errors,function(indice,valor){
                       errorCompleto = errorCompleto + valor+' ' ;                       
                    });
                    swal("Opss..., Hubo un error!",errorCompleto,"error");
                }
            });
        });

        //LISTAR EVALUACIONES PROVEEDOR
        function MostrarEvaluacion(btn){
            var route = "ListarEvalProv/"+btn.value;
            $.get(route, function(res){
                console.log(res[0]);
                if (typeof(res[0]) === "undefined") {
                    console.log('No Existe Datos');
                    $("#TableEvaluaciones tr td").remove(); 
                }
                else{
                    console.log('Existe Datos');            
                    //$("#TableEvaluaciones tr td").remove(); 
                    //var nro = 0;
                    $( "#TableEvaluaciones tbody tr" ).each( function(){ this.parentNode.removeChild( this ); }); 
                    for (i = 0; i < res.length; i++){
                        //nro = nro +1;
                             $("#TableEvaluaciones").append('<tr class="items_columsReceta3">' + 
                                '<td align="center" style="dislay: none;"><input type="text" name="nro[]" class="form-control" readonly value="'+res[i].eval_id+ '"></input></td>'+
                                '<td align="center" style="dislay: none;"><input type="text" name="nro[]" class="form-control" readonly value="'+desc(res[i].eval_evaluacion) + '"></input></td>'+'</tr>');
                        }
                    itemAux = res;
                        
                    
                }
                
            });
        }

        function desc(id){
            if (id == 1) {
                return 'Entrego muy tarde';
            }else if(id == 2){
                return 'Entrego productos dañados';
            }else{
                return 'No Entrego';
            } 
        }
</script>
<?php $__env->stopPush(); ?>



<?php echo $__env->make('backend.template.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>