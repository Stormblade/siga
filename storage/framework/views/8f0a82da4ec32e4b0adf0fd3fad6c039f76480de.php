<div class="modal fade modal-primary" data-backdrop="static" data-keyboard="false" id="myCreateIns" tabindex="-5">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel">
                    Registro Insumo
                </h4>
            </div>
            <div class="modal-body">
                <div class="caption">
                        <?php echo Form::open(['id'=>'proveedor']); ?>

                        <input id="token" name="csrf-token" type="hidden" value="<?php echo e(csrf_token()); ?>">
                            <input id="id" name="provid" type="hidden" value="">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Nombre Genérico:
                                                </label> 
                                                <?php echo Form::textarea('res.descripcion', null, array('placeholder' => ' ','class' => 'form-control','id'=>'descripcion', 'rows'=>'2','required','style'=>'text-transform:uppercase;', 'onkeyup'=>'javascript:this.value=this.value.toUpperCase();')); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Tipo Insumo:
                                                </label>
                                                <select class="form-control" id="id_tip_ins" name="id_tip_ins" placeholder="" value="" onchange="muestradat()">
                                                    <option value="">Seleccione...</option>
                                                    <?php $__currentLoopData = $dataIns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ins): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($ins->tins_id); ?>"><?php echo e($ins->tins_nombre); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Partida:
                                                </label>
                                                <select class="form-control" id="id_part" name="id_part" placeholder="" value="">
                                                    <option value="">Seleccione...</option>
                                                    <?php $__currentLoopData = $dataPart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $part): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($part->part_id); ?>"><?php echo e($part->part_nombre); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tipo_envase" style="display: none;">
                                    <div class="row">
                                        <h4 class="text-center" style="color:#2067b4"><strong>ENVASE</strong></h4>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <label>
                                                        Tipo Envase:
                                                    </label>
                                                    <select class="form-control" id="id_tip_env" name="id_tip_env" placeholder="" value="">
                                                        <option value="">Selec. tipo envase...</option>
                                                        <?php $__currentLoopData = $dataTipEnv; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tipenv): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($tipenv->tenv_id); ?>"><?php echo e($tipenv->tenv_nombre); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <label>
                                                       Linea producción:
                                                    </label>
                                                    <select class="form-control" id="id_linea_prod" name="id_linea_prod" value="">
                                                        <option value="">Selec. linea Prod</option>
                                                        <?php $__currentLoopData = $dataLineaProd; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lineaProd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($lineaProd->linea_prod_id); ?>"><?php echo e($lineaProd->linea_prod_nombre); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <label>
                                                        Producto Específico:
                                                    </label>
                                                    <select class="form-control" id="id_prod_especifico" name="id_prod_especifico" placeholder="" value="">
                                                        <option value="">Seleccione prod. especif...</option>
                                                        <?php $__currentLoopData = $dataProdEspe; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prodEsp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($prodEsp->prod_esp_id); ?>"><?php echo e($prodEsp->prod_esp_nombre); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <label>
                                                        Mercado:
                                                    </label>
                                                    <select class="form-control" id="id_mercado" name="id_mercado" placeholder="" value="">
                                                        <option value="">Seleccione mercado...</option>
                                                        <?php $__currentLoopData = $dataMercado; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mercado): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($mercado->mer_id); ?>"><?php echo e($mercado->mer_nombre); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <label>
                                                       Formato:
                                                    </label>
                                                    <?php echo Form::text('formato', null, array('placeholder' => ' ','class' => 'form-control','id'=>'formato', 'rows'=>'2','required','style'=>'text-transform:uppercase;', 'onkeyup'=>'javascript:this.value=this.value.toUpperCase();','placeholder'=>'Formato')); ?>

                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <label>
                                                       Unidad Medida:
                                                    </label>
                                                    <select class="form-control" id="id_uni" name="id_uni" value="">
                                                        <option value="">Seleccione unidad medida</option>
                                                        <?php $__currentLoopData = $dataUni; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $uni): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($uni->umed_id); ?>"><?php echo e($uni->umed_nombre); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <label>
                                                       Municipio:
                                                    </label>
                                                    <select class="form-control" id="id_municipio" name="id_municipio" value="">
                                                        <option value="">Seleccione municipio</option>
                                                        <?php $__currentLoopData = $dataMunicipio; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $municipio): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($municipio->muni_id); ?>"><?php echo e($municipio->muni_nombre); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tipo_insumo" style="display: none;">
                                    <div class="row">
                                        <h4 class="text-center" style="color:#2067b4"><strong>INSUMO</strong></h4>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <label>
                                                        Sabor:
                                                    </label>
                                                    <select class="form-control" id="id_sabor" name="id_sabor" placeholder="" value="">
                                                        <option value="">Seleccione sabor</option>
                                                        <?php $__currentLoopData = $dataSabor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sabor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($sabor->sab_id); ?>"><?php echo e($sabor->sab_nombre); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <label>
                                                       Color:
                                                    </label>
                                                    <select class="form-control" id="id_color" name="id_color" value="">
                                                        <option value="">Seleccione color</option>
                                                        <?php $__currentLoopData = $dataColor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($color->col_id); ?>"><?php echo e($color->col_nombre); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <label>
                                                        Peso por presentación:
                                                    </label>
                                                    <?php echo Form::text('presentacion', null, array('placeholder' => ' ','class' => 'form-control','id'=>'presentacion', 'rows'=>'2','required','style'=>'text-transform:uppercase;', 'onkeyup'=>'javascript:this.value=this.value.toUpperCase();','placeholder'=>'Peso presentación')); ?>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <label>
                                                       Unidad Medida:
                                                    </label>
                                                    <select class="form-control" id="id_uni_ins" name="id_uni_ins" value="">
                                                        <option value="">Seleccione unidad medida</option>
                                                        <?php $__currentLoopData = $dataUni; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $uni): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($uni->umed_id); ?>"><?php echo e($uni->umed_nombre); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tipo_insumo_map" style="display: none;">
                                    <div class="row">
                                        <h4 class="text-center" style="color:#2067b4"><strong>INSUMO MATERIA PRIMA</strong></h4>
                                    </div>
                                    <div class="row">                                 
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <label>
                                                       Unidad Medida:
                                                    </label>
                                                    <select class="form-control" id="id_uni_ins_map" name="id_uni_ins_map" value="">
                                                        <option value="">Seleccione unidad medida</option>
                                                        <?php $__currentLoopData = $dataUni; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $uni): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($uni->umed_id); ?>"><?php echo e($uni->umed_nombre); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                 
                            </input>
                        </input>
                    </hr>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" type="button">
                    Cerrar
                </button>
                <?php echo link_to('#',$title='Registrar', $attributes=['id'=>'registroIns','class'=>'btn btn-success','style'=>''], $secure=null); ?>

                <?php echo Form::close(); ?>

            </div>
        </div>
    </div>
</div>

<?php $__env->startPush('scripts'); ?>
<script>
function muestradat(){

    var id_tipins = document.getElementById('id_tip_ins').value;
    console.log(id_tipins);
    
    if (id_tipins==1) {
        $('#tipo_insumo').show();
        $('#tipo_envase').hide();
        $('#tipo_insumo_map').hide();
        $('#id_tip_env').val("");
        $('#id_linea_prod').val("");
        $('#id_mercado').val("");
        $('#formato').val("");
        $('#id_uni').val("");
        $('#id_municipio').val("");
        $('#id_prod_especifico').val("");              
    }else if(id_tipins==2){      
       $('#tipo_envase').show();
       $('#tipo_insumo').hide();
       $('#tipo_insumo_map').hide();
       $('#id_sabor').val("");
       $('#id_color').val("");
       $('#presentacion').val("");
       $('#id_uni').val("");  
    }else if(id_tipins==3){
        $('#tipo_insumo').hide();
        $('#tipo_envase').hide();
        $('#tipo_insumo_map').show();
        $('#id_tip_env').val("");
        $('#id_linea_prod').val("");
        $('#id_mercado').val("");
        $('#formato').val("");
        $('#id_uni').val("");
        $('#id_municipio').val("");
        $('#id_prod_especifico').val("");
    }else{
        $('#tipo_insumo').show();
        $('#tipo_envase').hide();
        $('#tipo_insumo_map').hide();
        $('#id_tip_env').val("");
        $('#id_linea_prod').val("");
        $('#id_mercado').val("");
        $('#formato').val("");
        $('#id_uni').val("");
        $('#id_municipio').val("");
        $('#id_prod_especifico').val("");
    }
}
</script>
<?php $__env->stopPush(); ?> 

