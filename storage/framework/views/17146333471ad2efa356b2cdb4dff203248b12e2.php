 <?php $menuPlantas = app('siga\Http\Controllers\MenuController'); ?>
 <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Ocultar/Abrir menu lateral" style="color:#FFFFFF; width:6; height:6;"></div>
              </div>
            <!--logo start-->
            <a href="<?php echo e(url('/home')); ?>" class="logo"><img src="../img/siga_logo.png" alt="" width="50px"></a>
            <!--logo end-->
            <!--UFV-->
            <a href="#" class="logo ufv"><i class="fa fa-gg-circle" aria-hidden="true"></i> <strong>UFV: <?php echo e(Auth::user()->getUfv()); ?></strong></a>
            <!--END UFV-->
            <div class="top-menu">
                <ul class="nav navbar-nav navbar-right top-menu">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="glyphicon glyphicon-user"></span><?php echo e(Auth::user()->usr_usuario); ?><span class="caret"></span></a>
                <ul class="dropdown-menu dropdown-cart" role="menu">
              <li>
                <div class="text-center">
                  <span class="text-center">
                    
                        <!-- <img src="../img/demo/avatar.png" alt="" class="text-center" /> -->
                        <img src="/img/demo/avatar.png" alt="" style="height: 10vw; width: 10vw; border: 2px solid #fff;border-radius: 50%;box-shadow: 0 0 5px gray;display: inline-block;margin-left: auto;margin-right: auto;"/><br>
                        <span class="item-info">
                            <span><?php echo e(Session::get('NOMBRES')); ?> <?php echo e(Session::get('PATERNO')); ?> <?php echo e(Session::get('MATERNO')); ?></span><br>
                            <!-- <span><?php echo e(Session::get('ROL')); ?></span> -->
                            <span>Planta: <?php echo e(Session::get('PLANTA')); ?></span><br>
                            <span>Rol usuario: <?php echo e(Session::get('ROLUSUARIO')); ?></span>
                        </span>
                    
                </span>
               </div>
              </li>
              <li class="divider"></li>
              <li><a href="<?php echo e(route('cerrar')); ?>" class="btn btn-default btn-flat">Salir del Sistema</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <?php
      $idrol=Session::get('ID_ROL');
    ?>
    <?php if($idrol==8 or $idrol==9 or $idrol==22 or $idrol==29): ?>
    <div class="top-menu">
                <ul class="nav navbar-nav navbar-right top-menu">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <img src="/img/icono_plantas.png" width="25" height="25" alt=""> CAMBIO DE PLANTA<span class="caret"></span></a>
                <ul class="dropdown-menu dropdown-cart" role="menu" style="background: #0092ff">
              <li>
                  <!-- <span class="item">
                    <span class="item-left"> -->
                         <?php $__currentLoopData = $menuPlantas->menuPlantas(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menuPl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li style="border-bottom: solid 1px #000;"><a href="/CambioPlantas/<?php echo e($menuPl->id_planta); ?>" class="cambioPlantasAdminLinea"><img src="/img/icono_plantas.png" width="25" height="25" alt="">&nbsp;&nbsp;<span style="color: white"> <?php echo e($menuPl->nombre_planta); ?></span></a></li>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
                        
                   <!--  </span>
                </span> -->
              </li>
          </ul>
        </li>
      </ul>
    </div> 
    <?php endif; ?>
    <?php if($idrol==200): ?>
    <div class="top-menu">
                <ul class="nav navbar-nav navbar-right top-menu">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="glyphicon glyphicon-home"></span> PLANTAS FRUTOS<span class="caret"></span></a>
                <ul class="dropdown-menu dropdown-cart" role="menu">
              <li>
                  <span class="item">
                    <span class="item-left">
                         <?php $__currentLoopData = $menuPlantas->menuPlantasFrutos(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menuPl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><a href="/CambioPlantas/<?php echo e($menuPl->id_planta); ?>"><span class="glyphicon glyphicon-home"></span> <?php echo e($menuPl->nombre_planta); ?></a></li>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
                        
                    </span>
                </span>
              </li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="top-menu">
                <ul class="nav navbar-nav navbar-right top-menu">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="glyphicon glyphicon-home"></span> PLANTAS MIEL<span class="caret"></span></a>
                <ul class="dropdown-menu dropdown-cart" role="menu">
              <li>
                  <span class="item">
                    <span class="item-left">
                         <?php $__currentLoopData = $menuPlantas->menuPlantasMiel(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menuPl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><a href="/CambioPlantas/<?php echo e($menuPl->id_planta); ?>"><span class="glyphicon glyphicon-home"></span> <?php echo e($menuPl->nombre_planta); ?></a></li>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
                        
                    </span>
                </span>
              </li>
          </ul>
        </li>
      </ul>
    </div> 
    <div class="top-menu">
                <ul class="nav navbar-nav navbar-right top-menu">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="glyphicon glyphicon-home"></span> PLANTAS LACTEOS<span class="caret"></span></a>
                <ul class="dropdown-menu dropdown-cart" role="menu">
              <li>
                  <span class="item">
                    <span class="item-left">
                         <?php $__currentLoopData = $menuPlantas->menuPlantasLacteos(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menuPl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><a href="/CambioPlantas/<?php echo e($menuPl->id_planta); ?>"><span class="glyphicon glyphicon-home"></span> <?php echo e($menuPl->nombre_planta); ?></a></li>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
                        
                    </span>
                </span>
              </li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="top-menu">
                <ul class="nav navbar-nav navbar-right top-menu">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="glyphicon glyphicon-home"></span> PLANTAS ALMENDRA<span class="caret"></span></a>
                <ul class="dropdown-menu dropdown-cart" role="menu">
              <li>
                  <span class="item">
                    <span class="item-left">
                         <?php $__currentLoopData = $menuPlantas->menuPlantasAlmendra(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menuPl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><a href="/CambioPlantas/<?php echo e($menuPl->id_planta); ?>"><span class="glyphicon glyphicon-home"></span> <?php echo e($menuPl->nombre_planta); ?></a></li>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
                        
                    </span>
                </span>
              </li>
          </ul>
        </li>
      </ul>
    </div>   
    <?php endif; ?>   
</header>
<style type="text/css">
.ufv {
  width: 200px;
  transform: translateX(-50%);
  left: 50%;
  position: absolute;
}

</style>