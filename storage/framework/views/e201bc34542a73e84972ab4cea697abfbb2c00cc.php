<!DOCTYPE html>
<html lang="es">
    <?php echo $__env->make('backend.template.partials.htmlheader', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <body>
        <?php echo $__env->make('backend.template.partials.mainheader', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('backend.template.partials.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <section id="main-content">            
            <section class="wrapper">
                <?php echo $__env->yieldContent('main-content'); ?>
            </section>
        </section>
        <?php $__env->startSection('scripts'); ?>
            <?php echo $__env->make('backend.template.partials.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldSection(); ?>
    </body>
</html>