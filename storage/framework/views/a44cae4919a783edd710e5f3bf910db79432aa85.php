<?php $__env->startSection('main-content'); ?>
<?php 
    function stock_actualOP($id_insumo)
    {
        $planta = \DB::table('public._bp_usuarios')->join('public._bp_planta as planta','public._bp_usuarios.usr_planta_id','=','planta.id_planta')
                        ->select('planta.id_planta')->where('usr_id','=',Auth::user()->usr_id)->first();
        $stock_actual =DB::table('insumo.stock')->select(DB::raw('SUM(stock_cantidad) as stock_cantidad'))->where('stock_planta_id','=',$planta->id_planta)
                                ->where('stock_ins_id','=',$id_insumo)->first();
        return $stock_actual->stock_cantidad; 
    } 
 ?>
<div class="row">
    <div class="col-md-12">
        <div class="container col-lg-12" style="background: white;">        
            <?php $now = new DateTime('America/La_Paz'); ?>
            <div class="text-center">
                <h3 style="color:#2067b4"><strong>ENTREGA PEDIDO POR INSUMO ADICIONAL</strong></h3> 
            </div>
            <div class="text-center">
            	<h3>Código: ORP-<?php echo e($sol_orden_produccion->orprod_nro_orden); ?></h3>
            </div>
            <form action="<?php echo e(url('AprobacionSolAdicional')); ?>" class="form-horizontal" method="GET">
                <input id="token" name="csrf-token" type="hidden" value="<?php echo e(csrf_token()); ?>">
                <input id="fecha_resgistro" name="fecha_resgistro" type="hidden" value="<?php echo $now->format('d-m-Y H:i:s'); ?>">
                <input type="hidden" name="id_orp" id="nro_acopio" value="<?php echo e($sol_orden_produccion->orprod_id); ?>">
                    <div class="row"> 
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Producto:
                                    </label>                                   
                                    <input type="text" name="" value="<?php echo e($receta->rece_nombre); ?> <?php echo e($receta->sab_nombre); ?> <?php echo e($receta->rece_presentacion); ?> <?php echo e($receta->umed_nombre); ?>" class="form-control" readonly="true">        
                                </div>
                            </div>
                        </div>                                
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Cantidad a Producir:
                                    </label>
                                    <input type="text" value="<?php echo e($sol_orden_produccion->orprod_cantidad); ?>" name="" class="form-control" readonly="true">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Rendimiento Base:
                                    </label>
                                    <input type="text" value="<?php echo e($receta->rece_rendimiento_base); ?>" class="form-control" name="" readonly="true">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Observacion Pedido:
                                    </label>
                                    <textarea type="text" value="" class="form-control" name="" readonly="true"><?php echo e($sol_orden_produccion->orprod_obs_vodos); ?></textarea>
                                </div>
                            </div>
                        </div>                   
                                                                                    
                    </div>
                <?php if($receta->rece_lineaprod_id == 2 OR $receta->rece_lineaprod_id == 3): ?>
                    <div class="text">
                        <h4 style="color:#2067b4"><strong>MATERIA PRIMA</strong></h4> 
                    </div> 
                    <div class="row">
                        <div class="col-md-12">
                            		<?php                                     
	                                    $detalle_formulacion_map = \DB::table('insumo.detalle_receta')->join('insumo.insumo as ins','insumo.detalle_receta.detrece_ins_id','=','ins.ins_id')
	                                                                        ->join('insumo.unidad_medida as uni','ins.ins_id_uni','=','uni.umed_id')
	                                                                        ->where('ins_id_tip_ins',3)
	                                                                        ->where('detrece_rece_id',$receta->rece_id)->get();
	                                    
	                                    $calculos = $sol_orden_produccion->orprod_cantidad/$receta->rece_rendimiento_base;
                                    ?>
                                    <div class="form-group">
                                        <table  class="table table-hover small-text" id="TableRecetasMatPrim">
                                            <thead>
                                                <tr>
                                                    <th>Cod Insumo</th>
                                                    <th>Insumo</th>
                                                    <th>Unidad Medida</th>
                                                    <th>Cant. Base</th>
                                                    <th>Cantidad</th>
                                                                               
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $__currentLoopData = $detalle_formulacion_map; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dorp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($dorp->ins_codigo); ?></td>
                                                    <td><?php echo e($dorp->ins_desc); ?></td>
                                                    <td><?php echo e($dorp->umed_nombre); ?></td>
                                                    <td><?php echo e($dorp->detrece_cantidad); ?></td>
                                                    <td><?php echo e($dorp->detrece_cantidad*$calculos); ?></td>
                                                    
                                                </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                            </tbody>
                                        </table>


                                    </div>
                        </div>
                    </div>
                <?php endif; ?>
                
                <?php if($receta->rece_lineaprod_id==1 OR $receta->rece_lineaprod_id == 4 OR $receta->rece_lineaprod_id == 5): ?>
                    <div class="text">
                        <h4 style="color:#2067b4"><strong>FORMULACION DE LA BASE</strong></h4> 
                    </div> 
                    <div class="row">
                        <div class="col-md-12">
                                    <?php 
                                    
                                    $insumo_insumo = \DB::table('insumo.detalle_receta')->join('insumo.insumo as ins','insumo.detalle_receta.detrece_ins_id','=','ins.ins_id')
                                                        ->join('insumo.unidad_medida as uni','ins.ins_id_uni','=','uni.umed_id')
                                                        ->where('ins_id_tip_ins',1)
                                                        ->where('detrece_rece_id',$receta->rece_id)->get();
                                    $insumo_matprima = \DB::table('insumo.detalle_receta')->join('insumo.insumo as ins','insumo.detalle_receta.detrece_ins_id','=','ins.ins_id')
                                                                        ->join('insumo.unidad_medida as uni','ins.ins_id_uni','=','uni.umed_id')
                                                                        ->where('ins_id_tip_ins',3)
                                                                        ->where('detrece_rece_id',$receta->rece_id)->get();
                                    foreach ($insumo_insumo as $ins) {
                                        $detalle_formulacion[] = array("ins_id"=>$ins->ins_id,"ins_codigo"=>$ins->ins_codigo,"ins_desc"=>$ins->ins_desc, "umed_nombre"=>$ins->umed_nombre, "detrece_cantidad"=>$ins->detrece_cantidad);
                                    }
                                    foreach ($insumo_matprima as $ins) {
                                        $detalle_formulacion[] = array("ins_id"=>$ins->ins_id,"ins_codigo"=>$ins->ins_codigo,"ins_desc"=>$ins->ins_desc, "umed_nombre"=>$ins->umed_nombre, "detrece_cantidad"=>$ins->detrece_cantidad);
                                    }
                                    $calculos = $sol_orden_produccion->orprod_cantidad/$receta->rece_rendimiento_base;
                                    ?>
                                    <div class="form-group">
                                        <table  class="table table-hover small-text" id="TableRecetasBase">
                                            <thead>
                                                <tr>
                                                    <th>Cod Insumo</th>
                                                    <th>Insumo</th>
                                                    <th>Unidad Medida</th>
                                                    <th>Cant. Base</th>
                                                    <th>Cantidad</th>                                                    
                                                                               
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $__currentLoopData = $detalle_formulacion; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dorp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($dorp['ins_codigo']); ?></td>
                                                    <td><?php echo e($dorp['ins_desc']); ?></td>
                                                    <td><?php echo e($dorp['umed_nombre']); ?></td>
                                                    <td><?php echo e($dorp['detrece_cantidad']); ?></td>
                                                    <td><?php echo e($dorp['detrece_cantidad']*$calculos); ?></td>
                                                    
                                                </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>


                                    </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if($receta->rece_lineaprod_id == 1 OR $receta->rece_lineaprod_id == 4 OR $receta->rece_lineaprod_id == 5): ?>  
                    <div class="text">
                        <h4 style="color:#2067b4"><strong>SABORIZACIÓN</strong></h4> 
                    </div> 
                    <div class="row">
                        <div class="col-md-12">
                                    <?php 
                                    
                                    $detalle_formulacion = \DB::table('insumo.detalle_receta')->join('insumo.insumo as ins','insumo.detalle_receta.detrece_ins_id','=','ins.ins_id')
                                                                        ->join('insumo.unidad_medida as uni','ins.ins_id_uni','=','uni.umed_id')
                                                                        ->where('ins_id_tip_ins',4)
                                                                        ->where('detrece_rece_id',$receta->rece_id)->get();
                                   
                                    $calculos = $sol_orden_produccion->orprod_cantidad/$receta->rece_rendimiento_base;
                                    ?>
                                    <div class="form-group">
                                        <table  class="table table-hover small-text" id="TableRecetasSab">
                                            <thead>
                                                <tr>
                                                    <th>Cod Insumo</th>
                                                    <th>Insumo</th>
                                                    <th>Unidad Medida</th>
                                                    <th>Cant. Base</th>
                                                    <th>Cantidad</th>                                                    
                                                                                 
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php $__currentLoopData = $detalle_formulacion; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dorp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($dorp->ins_codigo); ?></td>
                                                    <td><?php echo e($dorp->ins_desc); ?></td>
                                                    <td><?php echo e($dorp->umed_nombre); ?></td>
                                                    <td><?php echo e($dorp->detrece_cantidad); ?></td>
                                                    <td><?php echo e($dorp->detrece_cantidad*$calculos); ?></td>
                                                    
                                                </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                            </tbody>
                                        </table>


                                    </div>
                        </div>
                    </div>
                <?php endif; ?>
                
                    <div class="text">
                        <h4 style="color:#2067b4"><strong>MATERIAL DE ENVASADO</strong></h4> 
                    </div> 
                    <div class="row">
                        <div class="col-md-12">
                                    <?php 
                                    
                                    $detalle_formulacion = \DB::table('insumo.detalle_receta')->join('insumo.insumo as ins','insumo.detalle_receta.detrece_ins_id','=','ins.ins_id')
                                                                        ->join('insumo.unidad_medida as uni','ins.ins_id_uni','=','uni.umed_id')
                                                                        ->where('ins_id_tip_ins',2)
                                                                        ->where('detrece_rece_id',$receta->rece_id)->get();
                                   
                                    $calculos = $sol_orden_produccion->orprod_cantidad/$receta->rece_rendimiento_base;
                                    ?>
                                    <div class="form-group">
                                        <table  class="table table-hover small-text" id="TableRecetasEnv">
                                            <thead>
                                                <tr>
                                                    <th>Cod Insumo</th>
                                                    <th>Insumo</th>
                                                    <th>Unidad Medida</th>
                                                    <th>Cant. Base</th>
                                                    <th>Cantidad</th>
                                                                                
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $__currentLoopData = $detalle_formulacion; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dorp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                       
                                                <tr>
                                                    <td><?php echo e($dorp->ins_codigo); ?></td>
                                                    <td><?php echo e($dorp->ins_desc); ?></td>
                                                    <td><?php echo e($dorp->umed_nombre); ?></td>
                                                    <td><?php echo e($dorp->detrece_cantidad); ?></td>
                                                    <td><?php echo e($dorp->detrece_cantidad*$calculos); ?></td>
                                                </tr>                                               
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                            </tbody>
                                        </table>
  

                                    </div>
                        </div>
                    </div>
                    <div class="text">
                        <h4 style="color:#2067b4"><strong>INSUMOS ADICIONALES</strong></h4> 
                    </div> 
                    <div class="row">
                        <div class="col-md-12">
                                    <?php 
                                    
                                    $detalle_formulacion = \DB::table('insumo.detalle_orden_produccion')->join('insumo.insumo as ins','insumo.detalle_orden_produccion.detorprod_ins_id','=','ins.ins_id')
                                                                        ->join('insumo.unidad_medida as uni','ins.ins_id_uni','=','uni.umed_id')
                                                                        //->where('ins_id_tip_ins',2)
                                                                        ->where('detorprod_orprod_id',$sol_orden_produccion->orprod_id)->get();
                                    //dd($detalle_formulacion);
                                    //$calculos = $sol_orden_produccion->orprod_cantidad/$receta->rece_rendimiento_base;
                                    ?>
                                    <div class="form-group">
                                        <table  class="table table-hover small-text" id="TableRecetasEnv">
                                            <thead>
                                                <tr>
                                                    <th>Cod Insumo</th>
                                                    <th>Insumo</th>
                                                    <th>Unidad Medida</th>
                                                    
                                                    <th>Cantidad</th>
                                                    
                                                    <th>Stock Actual</th>                               
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $__currentLoopData = $detalle_formulacion; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dorp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                       
                                                <tr>
                                                    <td><?php echo e($dorp->ins_codigo); ?></td>
                                                    <td><?php echo e($dorp->ins_desc); ?></td>
                                                    <td><?php echo e($dorp->umed_nombre); ?></td>
                                                    
                                                    <td><?php echo e($dorp->detorprod_cantidad); ?></td>
                                                    <?php if($dorp->detorprod_cantidad > stock_actualOP($dorp->ins_id)): ?>

                                                        <td style="background: #E99786"><?php echo e(stock_actualOP($dorp->ins_id)); ?></td>
                                                    <?php else: ?>
                                                        <td style="background: #84E53C"><?php echo e(stock_actualOP($dorp->ins_id)); ?></td>
                                                        <input type="hidden" value="verficaStock($dorp->detorprod_cantidad,stock_actualOP($dorp->ins_id));" name=""> 
                                                    <?php endif; ?>
                                                    <?php
                                                        $datos_stock[] = array('cantidadSol'=>$dorp->detorprod_cantidad,'cantidadStock'=>stock_actualOP($dorp->ins_id));
                                                    ?>
                                                </tr>                                               
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                            </tbody>
                                        </table>
  

                                    </div>
                        </div>
                    </div>
                               <div class="row">
                    
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Observaciones:
                                    </label>
                                    <textarea type="text" value="" class="form-control" name="obs_usr_aprob"></textarea>
                                </div>
                            </div>
                        </div> 
                </div>               
                    <br>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-right">
                            <a class="btn btn-danger btn-lg" href="<?php echo e(url('solRecibidas')); ?>" type="button">
                            Cerrar
                            </a>
                            <input type="submit"  value="Enviar a Producción" class="btn btn-success btn-lg">
                            </div>
                        </div>
                    </div>
                
            </form>
            
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<script>
$(document).ready(function() {
    verficaStock();
});
function verficaStock()
{
	var arrayJS=<?php echo json_encode($datos_stock);?>;
    console.log(arrayJS);
	for (var i = arrayJS.length - 1; i >= 0; i--) {
        var cantidadStock = parseInt(arrayJS[i].cantidadStock);
        var cantidadSol = parseInt(arrayJS[i].cantidadSol);
        console.log(cantidadSol);
		if (cantidadStock>=cantidadSol) {
			//console.log(arrayJS[i].cantidadStock);
		}else{
			console.log("No hay stock");
			swal("STOCK BAJO","En uno o mas de los insumos no existe la cantidad de stock disponible, por lo cual no podra aprobar esta solicitud","warning");
			$('input[type="submit"]').attr('disabled','disabled');
		}
	}
	
	
}
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('backend.template.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>