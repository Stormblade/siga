<?php $__env->startSection('main-content'); ?>
<?php echo $__env->make('backend.administracion.insumo.insumo_devolucion.devolucion_insumos.partials.modalCreate', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-default box-solid">
            <div class="box-header with-border">
            <div class="col-md-12">
                <div class="col-md-1">
                    <a type="button" class="btn btn-dark"  style="background: #000000;" href="<?php echo e(url('DevolucionRegistrosMenu')); ?>"><span class="fa fas fa-align-justify" style="background: #ffffff;"></span><h7 style="color:#ffffff">&nbsp;&nbsp;MENU</h7></a>
                </div>
                <div class="col-md-8">
                     <h4><label for="box-title">DEVOLUCIÓN INSUMO SOBRANTE</label></h4>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<br>

<div class="row">        
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border"></div>
                    <div class="box-body">
                        
                        <table class="col-md-12 table-bordered table-striped table-condensed cf" id="lts-devolucion">
                            <thead class="cf">
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        N° ORP
                                    </th>
                                    <th>
                                        Feha
                                    </th>
                                    <th>
                                        Fecha Dev.
                                    </th>
                                    <th>
                                        Producto
                                    </th>
                                    <th>
                                        Cantidad
                                    </th>
                                    <th>
                                        Estado
                                    </th>
                                </tr>
                            </thead>
                    </table>
                </div>    
            </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<script>
$(document).ready(function() {
      var table =$('#lts-devolucion').DataTable( {
         "processing": true,
            "serverSide": true,
            "ajax": "/DevolucionInsumo/create/",
            "columns":[
                {data: 'aprsol_id'},
                {data: 'acciones'},
                {data: 'aprsol_cod_solicitud'},
                {data: 'aprsol_gestion'},
                {data: 'aprsol_solicitud'},
                {data: 'aprsol_registrado'},
                {data: 'tipsol_nombre'},
            ],
        
        "language": {
             "url": "/lenguaje"
        },
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
         "order": [[ 0, "desc" ]]
    });
    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
    $('#buscanumsalida').on( 'keyup', function () {
    table
        .columns( 2 )
        .search( this.value )
        .draw();
    });

    $('#buscalote').on( 'keyup', function () {
    table
        .columns( 3 )
        .search( this.value )
        .draw();
    });
});

          

  function MostrarDataDev(btn){  
    var dat=btn.value;
    console.log(dat);
    $('#lts-devolucionDetalle').DataTable( {
            
            "destroy":true,
            "processing": true,
            "serverSide": true,
            "ajax": "DevolucionDetalle/"+dat,
            "columns":[
                {data: 'id_insumo'},
                {data: 'descripcion_insumo'},
                {data: 'unidad'},
                {data: 'cantidad'},
                {data: 'rango_adicional'},
                {data: 'adicion'},
                {data: 'devolucion'},
        ],
        "order": [[ 0, "asc" ]],
        "language": {
             "url": "/lenguaje"
        },
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });
    }

    $("#registroDev").click(function(){
           var rows = [];
            $('#lts-devolucionDetalle tbody tr').each(function(){
                 rows.push({
                     id:$(this).find("td").eq(0).html(),
                     insumo:$(this).find("td").eq(1).html(),
                     unidad:$(this).find("td").eq(2).html(),
                     cantidad:$(this).find("td").eq(3).html(),
                     rango:$(this).find("td").eq(4).html(),
                     adicion:$(this).find('td:eq(5) input').val(),
                     devolucion:$(this).find('td:eq(6) input').val(),
                    });
                });
            itemsDevoluciones = JSON.stringify(rows);
            console.log('Json Table devolucion: ', itemsDevoluciones);

            var route="/DevolucionInsumo";
                     var token =$("#token").val();
                     $.ajax({
                         url: route,
                         headers: {'X-CSRF-TOKEN': token},
                         type: 'POST',
                         dataType: 'json',
                         data: {
                         'id_aprsol':$("#aprsol_id").val(),
                         'num_sal':$("#num_saldia").val(),
                         'nom_rec':$("#dev_nombre").val(),
                         'data':itemsDevoluciones,
                         'obs':$("#obs").val(),
                         },
                         success: function(data){
                            // $('#lts-carrito').DataTable().ajax.reload();
                             swal("Devoluciones!", "registrado correctamente","success");
                             $("#myCreateDevolucion").modal('toggle');Limpiar();
                         },
                         error: function(result)
                         {
                         swal("Opss..!", "Error al registrar el dato", "error");
                         }
                     });
 });
   
    function MostrarDevolucion(btn){
      var route = "/DevolucionInsumo/"+btn.value+"/edit";
      $.get(route, function(res){
        $("#aprsol_id").val(res.aprsol_id);
        $("#num_saldia").val(res.aprsol_cod_solicitud+'/'+res.aprsol_gestion)
        $("#dev_nombre").val(res.rec_nombre);
      });
    }
</script>
<?php $__env->stopPush(); ?>



<?php echo $__env->make('backend.template.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>