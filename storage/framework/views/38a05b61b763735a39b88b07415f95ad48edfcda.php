<?php $__env->startSection('htmlheader_title'); ?>
	Home
<?php $__env->stopSection(); ?>

<?php if(Session::has('message')): ?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</botton>
	<?php echo e(Session::get('message')); ?>

</div>
<?php endif; ?>
<?php $__env->startSection('main-content'); ?>
	<div class="container spark-screen">
		<div class="row">
	<div class="paddingleft_right15">
                    <div class="row">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">
                                <h3 class="panel-title" style="font-family: 'Arial Black'">
                                    <i class="livicon" data-name="check" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> REGISTRO DEVOLUCIONES
                                </h3>
                                
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <?php
                                        $idrol=Session::get('ID_ROL');
                                    ?>
                                    <!--start-->
                                    <div class="col-sm-4 social-buttons">
                                
                                         <?php if($idrol==1 or $idrol==18): ?>
                                            <a href="<?php echo e(url('DevolucionInsumo')); ?>">
                                                <div class="small-box bg-blue-gradient efectoboton">
                                                    <div class="inner">
                                                      <h4>DEVOLUCIÓN</h4>
                                                      <p>INSUMO SOBRANTE</p>
                                                    </div>
                                                    <div class="icon efectoicon">
                                                      <img src="img/botones/sobrante.png" alt="" width="80">
                                                    </div>
                                                </div>
                                            </a>
                                         <?php endif; ?>
                                          <?php if($idrol==1 or $idrol==19): ?>
                                            <a href="<?php echo e(url('DevolucionDefectuoso')); ?>">
                                                <div class="small-box bg-blue-gradient efectoboton">
                                                    <div class="inner">
                                                      <h4>DEVOLUCIONES</h4>
                                                      <p>DEFECTUOSA</p>
                                                    </div>
                                                    <div class="icon efectoicon">
                                                      <img src="img/botones/devolucion recibida.png" alt="" width="80">
                                                    </div>
                                                </div>
                                            </a> 
                                         <?php endif; ?>    
                                         <?php if($idrol==1 or $idrol==19): ?>
                                            <a href="<?php echo e(url('DevolucionRecibida')); ?>">
                                                <div class="small-box bg-blue-gradient efectoboton">
                                                    <div class="inner">
                                                      <h4>DEVOLUCIONES</h4>
                                                      <p>RECIBIDAS</p>
                                                    </div>
                                                    <div class="icon efectoicon">
                                                      <img src="img/botones/devolucion recibida.png" alt="" width="80">
                                                    </div>
                                                </div>
                                            </a> 
                                         <?php endif; ?> 
                                    </div>
                                    <div class="col-sm-8">
                                        <img src="<?php echo e(asset('img/devoluciones.jpg')); ?>" width="100%" height="100%" alt="Imagen Registro de Insumos" class="img-responsive">
                                    </div>
                                    
                                </div>
                                <!--end-->
                            </div>
                        </div>
                    </div>
                </div>
</div>
	</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.template.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>