<?php $__env->startSection('main-content'); ?>
<?php echo $__env->make('backend.administracion.insumo.insumo_registro..proveedores.partials.modalCreate', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('backend.administracion.insumo.insumo_registro..proveedores.partials.modalUpdate', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-default box-solid">
            <div class="box-header with-border">
            <div class="col-md-12">
                <div class="col-md-8">
                     <h4><label for="box-title">INSUMOS KARDEX</label></h4>
                </div>
                <div class="col-md-2">
                </div>
                <div class="col-md-2">
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border"></div>
                    <div class="box-body">
                        <table class="col-md-12 table-bordered table-striped table-condensed cf" id="lts-inskardex">
                            <thead class="cf">
                                <tr>
                                    <th>  
                                       #                                   
                                    </th>
                                    <th>
                                        kardex Valorado
                                    </th>
                                    <th>
                                        Kardex Físico
                                    </th>
                                        Codigo Insumo
                                    <th>
                                        Insumo
                                    </th>
                                    <th>
                                        Unidad Medida
                                    </th>
                                    <th>
                                        Stock Actual
                                    </th>
                                </tr>
                            </thead>
                            <tr>
                            </tr>
                    </table>
                </div>    
            </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<script>
    var t = $('#lts-inskardex').DataTable( {
      
         "processing": true,
            "serverSide": true,
            "ajax": "/ListKardex/create/",
            "columns":[
                {data: 'ins_id'},
                {data: 'kardexValorado',orderable: false, searchable: false},
                {data: 'kardexFisico',orderable: false, searchable: false}, 
                {data: 'ins_desc'},
                {data: 'umed_nombre'},
                {data: 'stocks_cantidad'}
        ],
        
        "language": {
             "url": "/lenguaje"
        },
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
         "order": [[ 0, "desc" ]],
         
       
    });
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
</script>
<?php $__env->stopPush(); ?>



<?php echo $__env->make('backend.template.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>