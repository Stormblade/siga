<?php $__env->startSection('main-content'); ?>
<?php 
    function stock_actualOP($id_insumo)
    {
        $planta = \DB::table('public._bp_usuarios')->join('public._bp_planta as planta','public._bp_usuarios.usr_planta_id','=','planta.id_planta')
                        ->select('planta.id_planta')->where('usr_id','=',Auth::user()->usr_id)->first();
        $stock_actual =DB::table('insumo.stock')->select(DB::raw('SUM(stock_cantidad) as stock_cantidad'))->where('stock_planta_id','=',$planta->id_planta)
                                ->where('stock_ins_id','=',$id_insumo)->first();
        return $stock_actual->stock_cantidad; 
    } 
 ?>
<div class="row">
    <div class="col-md-12">
        <div class="container col-lg-12" style="background: white;">        
            <?php $now = new DateTime('America/La_Paz'); ?>
            <div class="text-center">
                <h3 style="color:#2067b4"><strong>ENTREGA PEDIDO TRASPASO</strong></h3> 
            </div>
            <div class="text-center">
            	<h3>Código: ORP-<?php echo e($sol_orden_produccion->orprod_nro_orden); ?></h3>
            </div>
            <form action="<?php echo e(url('AprobacionTraspaso')); ?>" class="form-horizontal" method="GET">
                <input id="token" name="csrf-token" type="hidden" value="<?php echo e(csrf_token()); ?>">
                <input id="fecha_resgistro" name="fecha_resgistro" type="hidden" value="<?php echo $now->format('d-m-Y H:i:s'); ?>">
                <input type="hidden" name="id_orp" id="nro_acopio" value="<?php echo e($sol_orden_produccion->orprod_id); ?>">
                    <div class="row">                
                       
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Observacion Pedido:
                                    </label>
                                    <textarea type="text" value="" class="form-control" name="" readonly="true"><?php echo e($sol_orden_produccion->orprod_obs_usr); ?></textarea>
                                </div>
                            </div>
                        </div>                   
                                                                                    
                    </div>
                
                    <div class="text">
                        <h4 style="color:#2067b4"><strong>INSUMOS SOLICITADOS</strong></h4> 
                    </div> 
                    <div class="row">
                        <div class="col-md-12">
                                <div class="form-group">
                                        <table  class="table table-hover small-text" id="TableRecetasEnv">
                                            <thead>
                                                <tr>
                                                    <th>Cod Insumo</th>
                                                    <th>Insumo</th>
                                                    <th>Unidad Medida</th>
                                                    <th>Cantidad</th>
                                                    <th>Stock Actual</th>                               
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $__currentLoopData = $detalle_sol_orp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dorp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                       
                                                <tr>
                                                    <td><?php echo e($dorp->ins_codigo); ?></td>
                                                    <td><?php echo e($dorp->ins_desc); ?></td>
                                                    <td><?php echo e($dorp->umed_nombre); ?></td>
                                                    <td><?php echo e($dorp->detorprod_cantidad); ?></td>                               
                                                    <?php if($dorp->detorprod_cantidad > stock_actualOP($dorp->ins_id)): ?>

                                                    	<td style="background: #E99786"><?php echo e(stock_actualOP($dorp->ins_id)); ?></td>
                                                    <?php else: ?>
                                                    	<td style="background: #84E53C"><?php echo e(stock_actualOP($dorp->ins_id)); ?></td>
                                                    	<input type="hidden" value="verficaStock($dorp->detorprod_cantidad,stock_actualOP($dorp->ins_id));" name=""> 
                                                    <?php endif; ?>
                                                    <?php
                                                    	$datos_stock[] = array('cantidadSol'=>$dorp->detorprod_cantidad,'cantidadStock'=>stock_actualOP($dorp->ins_id));
                                                    ?>
                                                </tr>                                               
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                            </tbody>
                                        </table>
  

                                    </div>
                        </div>
                    </div>
                               <div class="row">
                    
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>
                                        Observaciones:
                                    </label>
                                    <textarea type="text" value="" class="form-control" name="obs_usr_aprob"></textarea>
                                </div>
                            </div>
                        </div> 
                </div>               
                    <br>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-right">
                            <a class="btn btn-danger btn-lg" href="<?php echo e(url('solRecibidas')); ?>" type="button">
                            Cerrar
                            </a>
                            <!-- <?php echo link_to('#',$title='Registrar', $attributes=['id'=>'registro','class'=>'btn btn-success'], $secure=null); ?> -->
                            <input type="submit"  value="Enviar a Producción" class="btn btn-success btn-lg">
                            </div>
                        </div>
                    </div>
                
            </form>
            
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<script>
$(document).ready(function() {
    verficaStock();
});
function verficaStock()
{
	var arrayJS=<?php echo json_encode($datos_stock);?>;
    console.log(arrayJS);
	for (var i = arrayJS.length - 1; i >= 0; i--) {
        var cantidadStock = parseInt(arrayJS[i].cantidadStock);
        var cantidadSol = parseInt(arrayJS[i].cantidadSol);
        console.log(cantidadSol);
		if (cantidadStock>=cantidadSol) {
			//console.log(arrayJS[i].cantidadStock);
		}else{
			console.log("No hay stock");
			swal("STOCK BAJO","En uno o mas de los insumos no existe la cantidad de stock disponible, por lo cual no podra aprobar esta solicitud","warning");
			$('input[type="submit"]').attr('disabled','disabled');
		}
	}
	
}
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('backend.template.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>