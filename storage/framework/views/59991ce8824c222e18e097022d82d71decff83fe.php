<div class="modal fade bs-example-modal-sm in" data-backdrop="static" data-keyboard="false" id="myListEvaluacion" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                    ×
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Evaluación de Proveedor <span id="nombre_prov"></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="caption">
                    <div class="form-group">
                                    
                        <table  class="table table-hover small-text" id="TableEvaluaciones">
                            <thead>
                                <tr>
                                    <th>Nro</th>
                                    <th>Evaluación</th>                               
                                </tr>
                            </thead>
                            <tbody>
                                            
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" type="button">
                    Cerrar
                </button>
            </div>
        </div>
    </div>
</div>

                        

