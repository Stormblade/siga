<div class="modal fade modal-primary" data-backdrop="static" data-keyboard="false" id="myCreateReceta" tabindex="-5">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel">
                    REGISTRO RECETA
                </h4>
            </div>
            <div class="modal-body">
                <div class="caption">
                        <?php echo Form::open(['id'=>'proveedor', 'files' => true]); ?>

                        <input id="token" name="csrf-token" type="hidden" value="<?php echo e(csrf_token()); ?>">
                            <input id="id" name="provid" type="hidden" value="">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Nombre Receta:
                                                </label>
                                                <?php echo Form::text('nombre_receta', null, array('placeholder' => 'Nombre de la Receta','class' => 'form-control','id'=>'nombre_receta', 'style'=>'text-transform:uppercase;', 'onkeyup'=>'javascript:this.value=this.value.toUpperCase();')); ?>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                 <label>
                                                    Base/Cant. Mínima:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    <?php echo Form::number('cant_minima', null, array('placeholder' => 'Cant. minima','class' => 'form-control','id'=>'cant_minima')); ?>

                                                </span>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Unidad Base de Receta:
                                                </label>
                                                <?php echo Form::text('unidad_base', null, array('placeholder' => 'Unidad Base','class' => 'form-control','id'=>'unidad_base', 'style'=>'text-transform:uppercase;','placeholder'=>'BOLSAS, BOTELLAS, ETC.', 'onkeyup'=>'javascript:this.value=this.value.toUpperCase();')); ?>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Planta:
                                                </label>
                                                <select class="form-control" name="receta_planta" id="receta_planta">
                                                    <option>Seleccione</option>
                                                    <?php $__currentLoopData = $ls_plantas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $planta): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($planta->id_planta); ?>"><?php echo e($planta->nombre_planta); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select> 
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                 <label>
                                                    Producción:
                                                </label>
                                                <select class="form-control" name="receta_produccion" id="receta_produccion">
                                                    <option value="">Seleccione</option>
                                                    <?php $__currentLoopData = $lineaTrabajo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $linea): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($linea->ltra_id); ?>"><?php echo e($linea->ltra_nombre); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                 <label>
                                                    Mercado:
                                                </label>
                                                <select class="form-control" name="receta_mercado" id="receta_mercado">
                                                    <option value="">Seleccione</option>
                                                    <?php $__currentLoopData = $listarMercados; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mercado): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($mercado->merc_id); ?>"><?php echo e($mercado->merc_nombre); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>                               
                                <div class="">
                                    <a href="javascript:void(0);" style="font-size:18px;" id="addMore" title="Add More Person"><span class="btn btn-primary">Añadir Insumo</span></a>
                                </div>
                                <div class="form-group">
                                    
                                    <table  class="table table-hover small-text" id="tb">
                                        <tr class="tr-header">
                                            <th>Descripcion</th>
                                            <!--<th>Unidad</th>-->
                                            <th>Cantidad</th>
                                            <th>Opcion</th>
                                            <!-- <th>Rango Adicional</th>                                             -->
                                        <tr class="items_columsReceta2">
                                            <td><select name="descripcion[]" class="form-control">
                                                    <option value="">Seleccione</option>
                                                    <?php $__currentLoopData = $listarInsumo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $insumo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($insumo->ins_codigo.'+'.$insumo->ins_desc.'+'.$insumo->ins_id); ?>"><?php echo e($insumo->ins_codigo.' - '.$insumo->ins_desc.' - '.$insumo->unidad); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            <!--<td><select name="unidad[]" class="form-control">
                                                    <option value="">Seleccione</option>
                                                    <?php $__currentLoopData = $listarUnidades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $unidad): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($unidad->dat_nom); ?>"><?php echo e($unidad->dat_nom); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </td>-->
                                            <td><input type="text" name="cantidad[]" class="form-control"></td>
                                            <td><div class="text-center"><a href='javascript:void(0);'  class='remove btncirculo btn-md btn-danger'><i class="glyphicon glyphicon-remove"></i></a></div></td>
                                            <!-- <td><input type="text" name="rango[]" class="form-control"></td> -->
                                        </tr>
                                    </table>
                                </div>
                            </input>
                        </input>
                    </hr>
                </div>
            </div>       
            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" type="button">
                    Cerrar
                </button>
                <?php echo link_to('#',$title='Registrar', $attributes=['id'=>'registroReceta','class'=>'btn btn-success','style'=>''], $secure=null); ?>

                <?php echo Form::close(); ?>

            </div>
        </div>
    </div>
</div>

<datalist id="insumosRece" width="80px">
<?php $__currentLoopData = $listarInsumo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $insumo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($insumo->ins_desc); ?>"></option>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</datalist> 

<datalist id="unidades" width="80px">
<?php $__currentLoopData = $listarUnidades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $unidad): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($unidad->dat_nom); ?>"></option>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</datalist> 

<?php $__env->startPush('scripts'); ?>
<script type="text/javascript">

 $('#addMore').on('click', function() {
              var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
              data.find("input").val('');
     });
     $(document).on('click', '.remove', function() {
         var trIndex = $(this).closest("tr").index();
            if(trIndex>1) {
             $(this).closest("tr").remove();
           } else {
             swal('Lo siento','No puede borrar el unico item');
           }
      });
</script>
<?php $__env->stopPush(); ?>