<div class="modal fade modal-primary" data-backdrop="static" data-keyboard="false" id="myCreatePrima" tabindex="-5">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel">
                    Detalle Materia Prima 
                </h4>
            </div>
            <div class="modal-body">
                <div class="caption">
                        <?php echo Form::open(['id'=>'proveedor']); ?>

                        <input id="token" name="csrf-token" type="hidden" value="<?php echo e(csrf_token()); ?>">
                            <input id="envid" name="envid" type="hidden" value="">
                                <div class="row"> 
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                 <label>
                                                    Nombre:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    <?php echo Form::text('nombre_env', null, array('placeholder' => 'ANA CORTES','class' => 'form-control','id'=>'nombre_env', 'disabled'=>'true')); ?>

                                                </span>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row"> 
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                 <label>
                                                    Cantidad Total:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    <?php echo Form::number('cant_env', null, array('placeholder' => '5000','class' => 'form-control','id'=>'cant_env', 'disabled'=>'true')); ?>

                                                </span>  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                 <label>
                                                    Unidad:
                                                </label>
                                                 <select class="form-control" id="env_uni" name="env_uni" placeholder="" value="">
                                                    <option value="">Seleccione...</option>
                                                    <option value="1">KILO</option>
                                                    <option value="2">LITROS</option>
                                                </select> 
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                 <label>
                                                    Insumo:
                                                </label>
                                                <select class="form-control" id="ins_id" name="ins_id" placeholder="" value="">
                                                    <option value="">Seleccione...</option>
                                                    <?php $__currentLoopData = $combo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cmb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($cmb->ins_id); ?>"><?php echo e($cmb->ins_desc); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row"> 
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                 <label>
                                                    Cantidad Recibida:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    <?php echo Form::number('cantidad', null, array('placeholder' => 'Cantidad','class' => 'form-control','id'=>'cantidad')); ?>

                                                </span>  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                 <label>
                                                    Costo Unidad:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    <?php echo Form::number('costo', null, array('placeholder' => 'Costo','class' => 'form-control','id'=>'costo')); ?>

                                                </span>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Observacion General (Justificante)
                                                </label>
                                                <textarea id="env_obs" class="form-control"></textarea>
                                            </div>
                                        </div> 
                                    </div>                                   
                                </div>
                            </input>
                        </input>
                    </hr>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" type="button">
                    Cerrar
                </button>
                 <?php echo link_to('#',$title='Rechazar', $attributes=['id'=>'registroUfv','class'=>'btn btn-warning','style'=>''], $secure=null); ?>

                <?php echo Form::close(); ?>

                <?php echo link_to('#',$title='Aprobar', $attributes=['id'=>'registroMatAprob','class'=>'btn btn-success','style'=>''], $secure=null); ?>

                <?php echo Form::close(); ?>

            </div>
        </div>
    </div>
</div>

<?php $__env->startPush('scripts'); ?>
<script>
// $(document).ready(function() {
//         $('#registroUfv').click(function() {
//             // Recargo la página
//             location.reload();
//         });
//     });
</script>
<?php $__env->stopPush(); ?>

 

